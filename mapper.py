#!/usr/bin/env python
import sys


for line in sys.stdin:
    # on saute la premiere ligne
    if line.startswith("UNITE_NIVEAU1"):
        continue
    line = line.strip() #ceci permet de retirer les espaces en trop
    mots = line.split(",")
    # on va recuperer l'emploi et le NOMBRE_FEMME s'il y en a
    if len(mots) == 18: # on verifie qu'il y a bien 18 colonnes
        emploi = mots[7].strip('"') # on retire les guillemets
        nombre_femme = mots[13]# on recupere le nombre de femme
        if emploi and nombre_femme and (nombre_femme != "0"): #on s'assure d'avoir bien le nombre de femme et un emploi
            print("%s\t%s"%(emploi, nombre_femme))
   