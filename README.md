# PROJET I DE TRAITEMENT ET ANALYSE DE DONNEES MASSIVES

## Auteurs
- [DARIL RAOUL KENGNE WAMBO](  <a href="mailto:kengne_wambo.daril_raoul@courrier.uqam.ca">**```KEND82330000```**</a> )
- [Tina](  <u>**``` ```**</u> )
- [Samantha](  <u>**``` ```**</u> )
- [Yves](  <u>**``` ```**</u> )

## Description de la source de données

La source de données est un fichier csv de nom [**```ouverture_de_donnees_affichages_postulants.csv```**](ouverture_de_donnees_affichages_postulants.csv) disponible à l'adresse suivante : https://donnees.montreal.ca/dataset/offres-demploi-et-postulation. Ce fichier contient les données sur les offres d'emploi et les postulations de la ville de Montréal. Il est composé de 10 colonnes et de 1000 lignes. Les colonnes sont les suivantes comme fournies dans [le site de la ville de Montréal](https://donnees.montreal.ca/dataset/offres-demploi-et-postulation):
<!-- UNITE_NIVEAU1,DESCRIPTION_UNITE_NIVEAU1,UNITE,DESCRIPTION_UNITE,ACCREDITATION,DESCRIPTION_ACCREDITATION,TITRE,EMPLOI,NO_AFFICHAGE,DEBUT,FIN,INTERNE_EXTERNE,NOMBRE_POSTULANT,NOMBRE_FEMME,NOMBRE_HANDICAPE,NOMBRE_MINORITE_VISIBLE,NOMBRE_AUTOCHTONE,NOMBRE_MINORITE_ETHNIQUE -->


- **```Unite_Niveau1```**(Numérique) :  L'unité de niveau 1 représente le service ou l'arrondissement
- **```Description_Unite_Niveau1```**(Texte variable) : Description de l'unité de niveau 1
- **```Unite```** (Numérique) : L'unité administrative est l'unité d'affaires où l'offre d'emploi a été créée. Elle est représentée par 12 chiffres significatifs dont les 2 premiers représentent le service ou l'arrondissement. La numérotation représente une hiérarchie de la structure (ex.: Service, direction, division, section, ...)

- **`Description_Unite`** (Texte variable) : Description de l'unité administrative

- **`Accreditation`** (Numérique) : Accréditation de l'emploi de l'offre d'emploi. Elle est représentée par 2 chiffres.

- **`Description_Accreditation`** (Texte variable) : Description de l'accréditation

- **`Titre`** (Texte variable) : Description de l'emploi

- **`Emploi`** (Numérique) : Code d'emploi de l'offre d'emploi. Il est représenté par 6 chiffres.

- **`No_Affichage`** (Texte variable) : Numéro de l'offre d'emploi. C'est un champ alphanumérique avec un standard défini pour sa nomenclature. Ex.: FIN-16-TEMP-344210-1 Veut dire que c'est une offre d'emploi au service des finances faite en 2016 pour un poste temporaire sur l'emploi 344210. Dans certains cas le numéro de poste est aussi indiqué.

- **`Debut`** (Date) : Date de début de l'affichage

- **`Fin`** (Date) : Date de fin de l'affichage

- **`Interne_Externe`** (Texte variable) : Indicateur permettant de savoir si c'est un affichage ouvert à l'interne exclusivement.

- **`Nombre_Postulant`** (Numérique) : Nombre total de postulants ayant postulé sur cet affichage

- **`Nombre_Femme`** (Numérique) : Nombre de femmes ayant postulé sur cet affichage

- **`Nombre_Handicape`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que personne handicapée

- **`Nombre_Minorite_Visible`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que minorité visible

- **`Nombre_Autochtone`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant qu'autochtone

- **`Nombre_Minorite_Ethnique`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que minorité ethnique

## Visualisation des données
![image](Images/data_preview.png)




## Prétraitement des données
<!-- verification des donnees -->
<!-- avoir la duree de chaque affichage(creer la colone) -->

## Traitement MapReduce

Nous aimerons savoir **le nombre de postulantes(femme) par emploi**. Pour cela, nous allons suivre un ensemble d'étapes pour arriver à notre résultat à l'aide d'Hadoop streaming avec python.

### Mapper
Le mapper va nous permettre de faire un premier traitement sur les données. Il va nous permettre de récupérer les données qui nous intéressent ici un couple **(emploie où une femme a postulé,Nombre_Femme)** provenant d'un filtre qu'on fera sur chaque tuple de notre jeu de données retenant ceux ou le champ Nombre_Femme est non null ou superieure à 0 . 
Pour cela, nous allons utiliser le langage python pour écrire notre mapper. Le code est disponible dans le fichier [**```mapper.py```**](mapper.py). Le code est le suivant :

```python
#!/usr/bin/env python
import sys


for line in sys.stdin:
    # on saute la premiere ligne
    if line.startswith("UNITE_NIVEAU1"):
        continue
    line = line.strip() #ceci permet de retirer les espaces en trop
    mots = line.split(",")
    # on va recuperer l'emploi et le NOMBRE_FEMME s'il y en a
    if len(mots) == 18: # on verifie qu'il y a bien 18 colonnes
        emploi = mots[7].strip('"') # on retire les guillemets
        nombre_femme = mots[13]# on recupere le nombre de femme
        if emploi and nombre_femme and (nombre_femme != "0"): #on s'assure d'avoir bien le nombre de femme et un emploi
            print("%s\t%s"%(emploi, nombre_femme))
   
```
### Reducer
Le reducer va permettre de faire le second tratitement en faisant la somme des valeurs de Nombre_Femme pour chaque emploi. Le code est disponible dans le fichier [**```reducer.py```**](reducer.py). Le code est le suivant :

```python
#!/usr/bin/env python
import sys

result_map = {}

for line in sys.stdin:
    line = line.strip()
    emploi, nombre_femme = line.split("\t")
    if emploi in result_map:
        #On ajoute le nombre de femme au nombre de femme deja present
        result_map[emploi] += int(nombre_femme)
    else:
        #S'il n'y a pas encore d'emploi on l'ajoute
        result_map[emploi] = int(nombre_femme)

#Affichage du resultat sous le format emploi\tnombre_femme attendu par Hadoop
for emploi in result_map:
    print("%s\t%s"%(emploi, result_map[emploi]))
    

```
## Execution du job MapReduce

### Lancement du cluster Hadoop


Pour lancer le conteneur cloudera, entrer la commande suivante une fois positionné dans le dossier **inf8810_tp1_dyst** où se trouve le fichier [**```build_and_lunch_cloudera.sh```**](build_and_lunch_cloudera.sh):

```bash
./build_and_lunch_cloudera.sh
``` 

```bash
#!/bin/bash 
# Afficher en vert le texte
GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NOCOLOR='\033[0m'

# Afficher en vert  cyan le texte

echo -e "${CYAN}TP1: Construction du conteneur"
echo -e "${NOCOLOR}"
# aller a la ligne
echo ""
docker build -t cloudera_dyst .

echo -e "${GREEN}TP1: Conteneur construit \n"

echo -e "${CYAN}TP1: Lancement du conteneur et execution du job map reduce \n\n"

docker run --name cloudera_dyst --hostname=quickstart.cloudera --privileged=true -ti cloudera_dyst  /usr/bin/docker-quickstart
echo -e "${NOCOLOR}\n\n"
```

Ce script nous permet d'éffectuer deux opérations:
- **Construire l'image docker de notre job MapReduce**
  L'idée ici est de copier les données et les scripts dans l'image docker. Pour cela, nous avons utilisé le fichier [**```Dockerfile```**](Dockerfile) qui est le suivant :

```dockerfile
FROM cloudera/quickstart:latest
# On copie nos scripts et données dans le container
COPY ./ /root
```
- **Lancer le job MapReduce**
```bash
docker run --name cloudera_dyst --hostname=quickstart.cloudera --privileged=true -ti cloudera_dyst  /usr/bin/docker-quickstart

```
Cette commande va ouvrir le conteneur en tant que root en mode interactif et avec comme nom d'hôte **quickstart.cloudera**.

Patientez quelques minutes le temps que le conteneur se lance.



### Lancement du job MapReduce

Une fois le conteneur lancé, le terminal s'est ouvert, positionnez vous dans le dossier **/root** dans lequel les scripts et données ont été copiés. Pour cela, **entrez la commande suivante :**

```bash
cd /root
```

**Puis, entrez la commande suivante pour lancer le job MapReduce :**

```bash
./run_map_reduce_job.sh
```

Ce fichier est le suivant :

```bash
#!/bin/bash  -x

GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

#1.positionnement dans le repertoire /root/ qui a ete binde dans le docker-compose.yml
WORKDIR=/root
cd $WORKDIR
#2. Soyons sure que le fichier(ouverture_de_donnees_affichages_postulants.csv) existe sur hdfs
# En se referant a la documentation https://hadoop.apache.org/docs/r1.2.1/file_system_shell.html
echo -e "${CYAN}On verifie si le fichier ouverture_de_donnees_affichages_postulants.csv existe sur hdfs\n\n"



hdfs dfs -test -e ouverture_de_donnees_affichages_postulants.csv

# si le fichier existe deja sur hdfs, on ne fait rien, on continu sinon on le televerse sur hdfs
if [ $? -eq 0 ]; then
    echo -e "${GREEN}Skiping file upload...\n"
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv exists"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file ouverture_de_donnees_affichages_postulants.csv does not exist on hdfs, uploading it..."
    echo -e "${NOCOLOR}\n"


    hdfs dfs -put "${WORKDIR}/ouverture_de_donnees_affichages_postulants.csv" 

    
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv has been uploaded"
    echo -e "${NOCOLOR}\n"
fi




#on fait pareille pour les fichiers mapper et reducer
echo -e "${CYAN}"

hdfs dfs -test -e mapper.py

if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file mapper.py exists\n\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file mapper.py does not exist on hdfs, uploading it...\n"
    echo -e "${CYAN}\n"

    hdfs dfs -put "${WORKDIR}/mapper.py" 

    echo -e "${GREEN}The file mapper.py has been uploaded\n\n"
    echo -e "${NOCOLOR}"
fi

hdfs dfs -test -e reducer.py
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload...\n"
    echo -e "${GREEN}The file reducer.py exists\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file reducer.py does not exist on hdfs, uploading it..."
    echo -e "${CYAN}\n\n"
    

    hdfs dfs -put "${WORKDIR}/reducer.py" 

    echo -e "${GREEN}The file reducer.py has been uploaded\n"
    echo -e "${NOCOLOR}\n\n"
    
fi


#3. On s'assure que le fichier de resultat n'existe pas deja sur hdfs
hdfs dfs -test -e /user/cloudera/resultat_traitement_1
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file resultat_traitement_1 exists"
    echo -e "${CYAN}\n\n"

    hdfs dfs -rm -r /user/cloudera/resultat_traitement_1


    echo -e "${GREEN}The file resultat_traitement_1 has been deleted"
    echo -e "${NOCOLOR}\n\n"
else
    echo -e "${GREEN}The file resultat_traitement_1 does not exist on hdfs, skiping..."
    echo -e "${NOCOLOR}\n\n"
fi

#3. On lance le job map reduce

echo -e "${CYAN}TP1: Lancement du job map reduce du calcul du nombre de femme qui postulent par emploi"

hadoop jar '/usr/lib/hadoop-mapreduce/hadoop-streaming.jar' -file 'mapper.py' -mapper mapper.py -file 'reducer.py' -reducer reducer.py -input 'ouverture_de_donnees_affichages_postulants.csv' -output '/user/cloudera/resultat_traitement_1'

if [ $? -eq 0 ]; then
    echo -e "${GREEN}The job map reduce has been executed successfully\n\n"
    # Affichage du resultat
    echo -e "${CYAN}TP1: Affichage du resultat\n\n"

    hdfs dfs -cat /user/cloudera/resultat_traitement_1/part-00000

    echo -e "${NOCOLOR}"
    echo -e "${GREEN}TP1: Fin du traitement 1 \n\n"
    echo -e "${GREEN}Thaks! BY DYST\n\n"
    echo -e "${NOCOLOR}"
else
    echo -e "${RED}The job map reduce has failed"
    echo -e "${NOCOLOR}"
fi
```








