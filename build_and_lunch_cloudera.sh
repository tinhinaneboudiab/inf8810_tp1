#!/bin/bash 
# Afficher en vert le texte
GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NOCOLOR='\033[0m'

# Afficher en vert  cyan le texte

echo -e "${CYAN}TP1: Construction du conteneur"
echo -e "${NOCOLOR}"
# aller a la ligne
echo ""
docker build -t cloudera_dyst .

echo -e "${GREEN}TP1: Conteneur construit \n"

echo -e "${CYAN}TP1: Lancement du conteneur et execution du job map reduce \n\n"

docker run --name cloudera_dyst --hostname=quickstart.cloudera --privileged=true -ti cloudera_dyst  /usr/bin/docker-quickstart
echo -e "${NOCOLOR}\n\n"

