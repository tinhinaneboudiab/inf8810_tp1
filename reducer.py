#!/usr/bin/env python
import sys

result_map = {}

for line in sys.stdin:
    line = line.strip()
    emploi, nombre_femme = line.split("\t")
    if emploi in result_map:
        #On ajoute le nombre de femme au nombre de femme deja present
        result_map[emploi] += int(nombre_femme)
    else:
        #S'il n'y a pas encore d'emploi on l'ajoute
        result_map[emploi] = int(nombre_femme)

#Affichage du resultat sous le format emploi\tnombre_femme attendu par Hadoop
for emploi in result_map:
    print("%s\t%s"%(emploi, result_map[emploi]))
    
