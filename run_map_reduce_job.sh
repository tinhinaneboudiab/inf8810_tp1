#!/bin/bash  -x

GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

#1.positionnement dans le repertoire /root/ qui a ete binde dans le docker-compose.yml
WORKDIR=/root
cd $WORKDIR
#2. Soyons sure que le fichier(ouverture_de_donnees_affichages_postulants.csv) existe sur hdfs
# En se referant a la documentation https://hadoop.apache.org/docs/r1.2.1/file_system_shell.html
echo -e "${CYAN}On verifie si le fichier ouverture_de_donnees_affichages_postulants.csv existe sur hdfs\n\n"



hdfs dfs -test -e ouverture_de_donnees_affichages_postulants.csv

# si le fichier existe deja sur hdfs, on ne fait rien, on continu sinon on le televerse sur hdfs
if [ $? -eq 0 ]; then
    echo -e "${GREEN}Skiping file upload...\n"
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv exists"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file ouverture_de_donnees_affichages_postulants.csv does not exist on hdfs, uploading it..."
    echo -e "${NOCOLOR}\n"


    hdfs dfs -put "${WORKDIR}/ouverture_de_donnees_affichages_postulants.csv" 

    
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv has been uploaded"
    echo -e "${NOCOLOR}\n"
fi




#on fait pareille pour les fichiers mapper et reducer
echo -e "${CYAN}"

hdfs dfs -test -e mapper.py

if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file mapper.py exists\n\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file mapper.py does not exist on hdfs, uploading it...\n"
    echo -e "${CYAN}\n"

    hdfs dfs -put "${WORKDIR}/mapper.py" 

    echo -e "${GREEN}The file mapper.py has been uploaded\n\n"
    echo -e "${NOCOLOR}"
fi

hdfs dfs -test -e reducer.py
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload...\n"
    echo -e "${GREEN}The file reducer.py exists\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file reducer.py does not exist on hdfs, uploading it..."
    echo -e "${CYAN}\n\n"
    

    hdfs dfs -put "${WORKDIR}/reducer.py" 

    echo -e "${GREEN}The file reducer.py has been uploaded\n"
    echo -e "${NOCOLOR}\n\n"
    
fi


#3. On s'assure que le fichier de resultat n'existe pas deja sur hdfs
hdfs dfs -test -e /user/cloudera/resultat_traitement_1
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file resultat_traitement_1 exists"
    echo -e "${CYAN}\n\n"

    hdfs dfs -rm -r /user/cloudera/resultat_traitement_1


    echo -e "${GREEN}The file resultat_traitement_1 has been deleted"
    echo -e "${NOCOLOR}\n\n"
else
    echo -e "${GREEN}The file resultat_traitement_1 does not exist on hdfs, skiping..."
    echo -e "${NOCOLOR}\n\n"
fi

#3. On lance le job map reduce

echo -e "${CYAN}TP1: Lancement du job map reduce du calcul du nombre de femme qui postulent par emploi"

hadoop jar '/usr/lib/hadoop-mapreduce/hadoop-streaming.jar' -file 'mapper.py' -mapper mapper.py -file 'reducer.py' -reducer reducer.py -input 'ouverture_de_donnees_affichages_postulants.csv' -output '/user/cloudera/resultat_traitement_1'

if [ $? -eq 0 ]; then
    echo -e "${GREEN}The job map reduce has been executed successfully\n\n"
    # Affichage du resultat
    echo -e "${CYAN}TP1: Affichage du resultat\n\n"

    hdfs dfs -cat /user/cloudera/resultat_traitement_1/part-00000

    echo -e "${NOCOLOR}"
    echo -e "${GREEN}TP1: Fin du traitement 1 \n\n"
    echo -e "${GREEN}Thaks! BY DYST\n\n"
    echo -e "${NOCOLOR}"
else
    echo -e "${RED}The job map reduce has failed"
    echo -e "${NOCOLOR}"
fi
